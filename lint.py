#!/usr/bin/env python
# -*- coding: utf-8 -*-
#0.1.0
import os
import sys
import os.path
import shutil

def main():
    # os.getcwd() 方法用于返回当前工作目录
    current_path = os.getcwd()
    print('当前所在路径：' + current_path)
    print('')

    os.system('pod lib lint --sources=\'https://github.com/CocoaPods/Specs.git,https://gitlab.com/Malygos/zcprepo.git\'')
    # os.system('pod spec lint --sources=\'https://github.com/CocoaPods/Specs.git,https://gitlab.com/Malygos/zcprepo.git\'')
    # os.system('pod repo push gitlab-malygos-zcprepo ZCPUIKit.podspec --sources=\'https://github.com/CocoaPods/Specs.git,https://gitlab.com/Malygos/zcprepo.git\'')
if __name__ == '__main__':
	main()