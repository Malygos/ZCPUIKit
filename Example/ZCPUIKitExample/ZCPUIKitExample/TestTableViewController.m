//
//  TestTableViewController.m
//  ZCPUIKit
//
//  Created by zcp on 2019/1/13.
//  Copyright © 2019 zcp. All rights reserved.
//

#import "TestTableViewController.h"
#import <ZCPUIKit.h>
#import <ZCPGlobal.h>
#import <ZCPCategory.h>

@interface TestTableViewController ()

@end

@implementation TestTableViewController

// ----------------------------------------------------------------------
#pragma mark - life cycle
// ----------------------------------------------------------------------

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    self.tableView.frame = self.view.bounds;
}

#pragma mark - data

- (void)constructData {
    NSMutableArray *items = [NSMutableArray array];
    
    // 空cell
    [items addObject:[self generateSplitLineCellItem]];
    {
        ZCPTableViewCellDataModel *item = [[ZCPTableViewCellDataModel alloc] init];
        item.cellClass = [ZCPTableViewCell class];
        item.cellHeight = @50;
        item.cellBgColor = [UIColor colorFromHexRGB:@"FFF5EE"];
        [items addObject:item];
    }
    [items addObject:[self generateSplitLineCellItem]];
    // 带上下边线的cell
    {
        ZCPTableViewCellDataModel *item = [[ZCPTableViewCellDataModel alloc] init];
        item.cellClass = [ZCPTableViewCell class];
        item.cellHeight = @50;
        item.cellBgColor = [UIColor colorFromHexRGB:@"FFF5EE"];
        item.showTopLine = YES;
        item.topLineOffset = 16;
        item.topLineLength = SCREENWIDTH - 16;
        item.topLineColor = [UIColor redColor];
        item.showBottomLine = YES;
        item.bottomLineOffset = 16;
        item.bottomLineLength = SCREENWIDTH - 16;
        item.bottomLineColor = [UIColor blueColor];
        [items addObject:item];
    }
    [items addObject:[self generateSplitLineCellItem]];
    // 带title的cell
    {
        ZCPSectionCellItem *item = [[ZCPSectionCellItem alloc] initWithDefault];
        item.sectionTitlePosition = ZCPSectionTitleTopPosition | ZCPSectionTitleMiddlePosition;
        item.sectionTitle = @"我爱中国！";
        item.sectionTitlePosition = ZCPSectionTitleLeftPosition | ZCPSectionTitleTopPosition;
        item.sectionTitleEdgeInsets = UIEdgeInsetsMake(5, 0, 0, 0);
        item.cellHeight = @50;
        item.cellBgColor = [UIColor colorFromHexRGB:@"FFF5EE"];
        [items addObject:item];
    }
    [items addObject:[self generateSplitLineCellItem]];
    // 带按钮的cell
    {
        ZCPButtonCellItem *item = [[ZCPButtonCellItem alloc] initWithDefault];
        item.buttonTitle = @"提交";
        item.buttonBackgroundColor = [UIColor colorFromHexRGB:@"ff8447"];
        item.cellBgColor = [UIColor colorFromHexRGB:@"FFF5EE"];
        [items addObject:item];
    }
    [items addObject:[self generateSplitLineCellItem]];
    // 带输入框的cell
    {
        ZCPTextFieldCellItem *item = [[ZCPTextFieldCellItem alloc] initWithDefault];
        item.cellBgColor = [UIColor colorFromHexRGB:@"FFF5EE"];
        item.placeholder = @"请输入内容...";
        item.cellHeight = @50;
        [items addObject:item];
    }
    [items addObject:[self generateSplitLineCellItem]];
    // 带输入域的cell
    {
        ZCPTextViewCellItem *item = [[ZCPTextViewCellItem alloc] initWithDefault];
        item.cellBgColor = [UIColor colorFromHexRGB:@"FFF5EE"];
        item.placeholder = @"请输入内容...";
        item.textEdgeInset = UIEdgeInsetsMake(20, 20, 20, 20);
        item.cellHeight = @100;
        [items addObject:item];
    }
    [items addObject:[self generateSplitLineCellItem]];
    self.tableViewAdaptor.items = items;
}

- (ZCPTableViewCellDataModel *)generateSplitLineCellItem {
    ZCPTableViewCellDataModel *item = [[ZCPTableViewCellDataModel alloc] init];
    item.cellBgColor = [UIColor whiteColor];
    item.cellClass = [ZCPTableViewCell class];
    item.cellHeight = @10;
    return item;
}

// ----------------------------------------------------------------------
#pragma mark - tableview
// ----------------------------------------------------------------------

- (void)tableView:(UITableView *)tableView didSelectObject:(id<ZCPTableViewCellItemBasicProtocol>)object rowAtIndexPath:(NSIndexPath *)indexPath {
    
}

// ----------------------------------------------------------------------
#pragma mark - getters and setters
// ----------------------------------------------------------------------



@end
