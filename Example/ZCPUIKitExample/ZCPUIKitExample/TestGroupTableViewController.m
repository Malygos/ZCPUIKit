//
//  TestGroupTableViewController.m
//  ZCPUIKit
//
//  Created by zcp on 2019/1/14.
//  Copyright © 2019 zcp. All rights reserved.
//

#import "TestGroupTableViewController.h"
#import <ZCPCategory.h>

@interface TestGroupTableViewController ()

@end

@implementation TestGroupTableViewController

#pragma mark - life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.tableView.frame = self.view.bounds;
}

#pragma mark - data

- (void)constructData {
    NSMutableArray *items = [NSMutableArray array];
    NSArray *sections = @[@[@"ffecec", @"ffd2d2", @"ffb5b5", @"ff9797",
                            @"ff7575", @"ff5151", @"ff2d2d", @"ff0000",
                            @"ea0000", @"ce0000", @"ae0000", @"930000",
                            @"750000", @"600000", @"4d0000", @"2f0000"],
                          @[@"fff3ee", @"ffe6d9", @"ffdac8", @"ffcbb3",
                            @"ffbd9d", @"ffad86", @"ff9d6f", @"ff8f59",
                            @"ff8040", @"ff5809", @"f75000", @"d94600",
                            @"bb3d00", @"a23400", @"842b00", @"642100"],
                          @[@"fffff4", @"ffffdf", @"ffffce", @"ffffb9",
                            @"ffffaa", @"ffff93", @"ffff6f", @"ffff37",
                            @"f9f900", @"e1e100", @"c4c400", @"a6a600",
                            @"8c8c00", @"737300", @"5b5b00", @"424200"],
                          @[@"f5ffe8", @"efffd7", @"e8ffc4", @"deffac",
                            @"d3ff93", @"ccff80", @"c2ff68", @"b7ff4a",
                            @"a8ff24", @"9aff02", @"8cea00", @"82d900",
                            @"73bf00", @"64a600", @"548c00", @"467500"],
                          @[@"fdffff", @"ecffff", @"d9ffff", @"caffff",
                            @"bbffff", @"a6ffff", @"80ffff", @"4dffff",
                            @"00ffff", @"00e3e3", @"00caca", @"00aeae",
                            @"009393", @"007979", @"005757", @"003e3e"],
                          @[@"ecf5ff", @"d2e9ff", @"c4e1ff", @"acd6ff",
                            @"97cbff", @"84c1ff", @"66b3ff", @"46a3ff",
                            @"2894ff", @"0080ff", @"0072e3", @"0066cc",
                            @"005ab5", @"004b97", @"003d79", @"000079"],
                          @[@"faf4ff", @"f1e1ff", @"f6caff", @"dcb5ff",
                            @"d3a4ff", @"ca8eff", @"be77ff", @"b15bff",
                            @"9f35ff", @"921aff", @"8600ff", @"6f00d2",
                            @"5b00ae", @"4b0091", @"3a006f", @"28004d"],];
    
    for (NSArray *colors in sections) {
        // section
        ZCPTableViewGroupDataModel *section = [[ZCPTableViewGroupDataModel alloc] init];
        [items addObject:section];
        // items
        for (NSString *colorStr in colors) {
            UIColor *color = [UIColor colorFromHexRGB:colorStr];
            ZCPTableViewCellDataModel *item = [[ZCPTableViewCellDataModel alloc] init];
            item.cellClass = [ZCPTableViewCell class];
            item.cellHeight = @50;
            item.cellBgColor = color;
            [section.sectionCellItems addObject:item];
        }
    }
    self.tableViewAdaptor.items = items;
}

- (BOOL)isMultipleGroups {
    return YES;
}

// ----------------------------------------------------------------------
#pragma mark - tableview
// ----------------------------------------------------------------------

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section objectForHeader:(id)object {
    UILabel *label = [[UILabel alloc] init];
    NSArray *titles = @[@"红", @"橙", @"黄", @"绿", @"青", @"蓝", @"紫"];
    label.text = titles[section];
    label.backgroundColor = [UIColor whiteColor];
    return label;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section objectForHeader:(id)object {
    return 50;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section objectForFooter:(id)object {
    UILabel *label = [[UILabel alloc] init];
    NSArray *titles = @[@"红", @"橙", @"黄", @"绿", @"青", @"蓝", @"紫"];
    NSArray *colors = @[@"2f0000", @"642100", @"424200", @"467500", @"003e3e", @"000079", @"28004d"];
    label.text = titles[section];
    label.textColor = [UIColor whiteColor];
    label.backgroundColor = [UIColor colorFromHexRGB:colors[section]];
    return label;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section objectForFooter:(id)object {
    return 50;
}

@end
