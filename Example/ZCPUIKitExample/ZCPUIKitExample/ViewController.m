//
//  ViewController.m
//  ZCPUIKitExample
//
//  Created by zcp on 2019/4/16.
//  Copyright © 2019 zcp. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) NSArray *testArr;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.testArr = @[@{@"title": @"list", @"cls": @"TestTableViewController"},
                     @{@"title": @"group", @"cls": @"TestGroupTableViewController"}];
}

// ----------------------------------------------------------------------
#pragma mark - UITableViewDataSource and UITableViewDelegate
// ----------------------------------------------------------------------

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.testArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    cell.textLabel.text = self.testArr[indexPath.row][@"title"];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *clsStr = self.self.testArr[indexPath.row][@"cls"];
    Class cls = NSClassFromString(clsStr);
    UIViewController *vc = [[cls alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}


@end
