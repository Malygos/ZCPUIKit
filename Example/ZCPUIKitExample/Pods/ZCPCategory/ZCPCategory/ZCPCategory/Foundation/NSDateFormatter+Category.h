//
//  NSDateFormatter+Category.h
//  ZCPKit
//
//  Created by zhuchaopeng on 16/9/18.
//  Copyright © 2016年 zcp. All rights reserved.
//

#import <Foundation/Foundation.h>

#pragma mark - 日期格式化单例对象

@interface NSDateFormatter (Category)

+ (nonnull instancetype)staticDateFormatter;

@end
