//
//  AppDelegate.h
//  ZCPUIKit
//
//  Created by zcp on 2019/4/16.
//  Copyright © 2019 zcp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

