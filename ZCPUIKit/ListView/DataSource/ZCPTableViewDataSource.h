//
//  ZCPTableViewDataSource.h
//  ZCPUIKit
//
//  Created by zhuchaopeng06607 on 2019/11/21.
//

#import <Foundation/Foundation.h>
#import "ZCPTableViewCell.h"
#import "ZCPTableViewCellViewModel.h"
#import "ZCPTableViewSectionDataModel.h"
@protocol ZCPTableViewDataSourceListenerProtocol;
@class ZCPTableViewDataSource;

NS_ASSUME_NONNULL_BEGIN

/// tableView 数据源对象协议
@protocol ZCPTableViewDataSourceProtocol <UITableViewDataSource>

/// section 数据
@property (nonatomic, strong) NSMutableArray <ZCPTableViewSectionDataModel *>*sectionDataModelArray;
/// 回调对象
@property (nonatomic, weak, nullable) id<ZCPTableViewDataSourceListenerProtocol> listener;

// 工具方法
/// 返回指定索引位置的 section 数据模型
- (nullable ZCPTableViewSectionDataModel *)dataModelForSectionInSection:(NSInteger)section;
/// 返回指定索引位置的 cell 视图模型
- (nullable ZCPTableViewCellViewModel *)viewModelForRowAtIndexPath:(NSIndexPath *)indexPath;
/// 返回 section 个数
- (NSInteger)numberOfSections;
/// 返回 指定 section 下的 cell 个数
/// @param section 指定section
- (NSInteger)numberOfRowsInSection:(NSInteger)section;

// 对UITableViewDelegate的补充
/// 返回指定索引位置的 cell 高度
- (CGFloat)heightForRowAtIndexPath:(NSIndexPath *)indexPath;
/// 返回指定索引位置的 section header 高度
- (CGFloat)heightForHeaderInSection:(NSInteger)section;
/// 返回指定索引位置的 section header 高度
- (CGFloat)heightForFooterInSection:(NSInteger)section;
/// 返回指定索引位置的 section header view
- (nullable UIView *)viewForHeaderInSection:(NSInteger)section;
/// 返回指定索引位置的 section footer view
- (nullable UIView *)viewForFooterInSection:(NSInteger)section;

@end

/// tableview 数据源对象监听者协议
@protocol ZCPTableViewDataSourceListenerProtocol <NSObject>

/// updateViewModel外部补充
/// @param dataSource 数据源对象
/// @param cell 当前需更新的 cell
/// @param viewModel 当前用于更新 cell 的 viewModel
- (void)tableViewDataSource:(ZCPTableViewDataSource *)dataSource didUpdateCell:(ZCPTableViewCell *)cell withViewModel:(ZCPTableViewCellViewModel *)viewModel;

@end

/// UITableView 数据源
@interface ZCPTableViewDataSource : NSObject <ZCPTableViewDataSourceProtocol>

@end

#undef UITableViewDelegate_ZCP_BASE_IMP
#define UITableViewDelegate_ZCP_BASE_IMP(__dataSource) \
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath { \
    return [__dataSource heightForRowAtIndexPath:indexPath]; \
} \
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section { \
    return [__dataSource heightForHeaderInSection:section]; \
} \
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section { \
    return [__dataSource viewForHeaderInSection:section]; \
}

NS_ASSUME_NONNULL_END
