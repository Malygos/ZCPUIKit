//
//  ZCPTableViewDataSource.m
//  ZCPUIKit
//
//  Created by zhuchaopeng06607 on 2019/11/21.
//

#import "ZCPTableViewDataSource.h"

@implementation ZCPTableViewDataSource

@synthesize sectionDataModelArray = _sectionDataModelArray;
@synthesize listener = _listener;

#pragma mark - util

- (ZCPTableViewSectionDataModel *)dataModelForSectionInSection:(NSInteger)section {
    if (self.sectionDataModelArray.count <= section) {
        return nil;
    }
    ZCPTableViewSectionDataModel *sectionDataModel = self.sectionDataModelArray[section];
    return sectionDataModel;
}

- (ZCPTableViewCellViewModel *)viewModelForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZCPTableViewSectionDataModel *sectionDataModel = [self dataModelForSectionInSection:indexPath.section];
    if (sectionDataModel.cellViewModelArray.count <= indexPath.row) {
        return nil;
    }
    ZCPTableViewCellViewModel *cellViewModel = sectionDataModel.cellViewModelArray[indexPath.row];
    return cellViewModel;
}

- (NSInteger)numberOfSections {
    CGFloat numberOfSections = self.sectionDataModelArray.count;
    return numberOfSections;
}

- (NSInteger)numberOfRowsInSection:(NSInteger)section {
    ZCPTableViewSectionDataModel *sectionDataModel = [self dataModelForSectionInSection:section];
    NSInteger numberOfRows = sectionDataModel.cellViewModelArray.count;
    return numberOfRows;
}

#pragma mark - public

- (CGFloat)heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZCPTableViewCellViewModel *cellViewModel = [self viewModelForRowAtIndexPath:indexPath];
    Class <ZCPTableViewCellProtocol>cellClass = cellViewModel.cellClass;
    CGFloat height = [cellClass heightForViewModel:cellViewModel];
    return height;
}

- (CGFloat)heightForHeaderInSection:(NSInteger)section {
    ZCPTableViewSectionDataModel *sectionDataModel = [self dataModelForSectionInSection:section];
    CGFloat height = sectionDataModel.headerViewModel.sectionViewHeight.floatValue;
    return height;
}

- (CGFloat)heightForFooterInSection:(NSInteger)section {
    ZCPTableViewSectionDataModel *sectionDataModel = [self dataModelForSectionInSection:section];
    CGFloat height = sectionDataModel.footerViewModel.sectionViewHeight.floatValue;
    return height;
}

- (UIView *)viewForHeaderInSection:(NSInteger)section {
    ZCPTableViewSectionDataModel *sectionDataModel = [self dataModelForSectionInSection:section];
    UIView <ZCPTableViewSectionViewProtocol> *headerView = nil;
    Class headerViewClass = sectionDataModel.headerViewModel.sectionViewClass;
    
    if (headerViewClass) {
        headerView = [[headerViewClass alloc] init];
    }
    if (headerView && [headerView respondsToSelector:@selector(updateWithViewModel:)]) {
        [headerView updateWithViewModel:sectionDataModel.headerViewModel];
    }
    return headerView;
}

- (UIView *)viewForFooterInSection:(NSInteger)section {
    ZCPTableViewSectionDataModel *sectionDataModel = [self dataModelForSectionInSection:section];
    UIView <ZCPTableViewSectionViewProtocol> *footerView = nil;
    Class footerViewClass = sectionDataModel.footerViewModel.sectionViewClass;
    
    if (footerViewClass) {
        footerView = [[footerViewClass alloc] init];
    }
    if (footerView && [footerView respondsToSelector:@selector(updateWithViewModel:)]) {
        [footerView updateWithViewModel:sectionDataModel.footerViewModel];
    }
    return footerView;
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self numberOfSections];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self numberOfRowsInSection:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZCPTableViewCellViewModel *cellViewModel = [self viewModelForRowAtIndexPath:indexPath];
    NSString *reuseIdentifier = cellViewModel.cellReuseIdentifier;
    ZCPTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    
    if (cell == nil) {
        cell = [ZCPTableViewCell cellForViewModel:cellViewModel];
    }
    
    [cell updateWithViewModel:cellViewModel];
    
    if (self.listener && [self.listener respondsToSelector:@selector(tableViewDataSource:didUpdateCell:withViewModel:)]) {
        [self.listener tableViewDataSource:self didUpdateCell:cell withViewModel:cellViewModel];
    }
    return cell;
}

#pragma mark - getters and setters

- (NSMutableArray<ZCPTableViewSectionDataModel *> *)sectionDataModelArray {
    if (!_sectionDataModelArray) {
        _sectionDataModelArray = [NSMutableArray array];
    }
    return _sectionDataModelArray;
}

@end
