//
//  ZCPTableViewSingleSectionDataSource.h
//  ZCPUIKit
//
//  Created by zhuchaopeng06607 on 2019/11/28.
//

#import "ZCPTableViewDataSource.h"

NS_ASSUME_NONNULL_BEGIN

/// 仅有一个 section 的 UITableView 数据源
@interface ZCPTableViewSingleSectionDataSource : ZCPTableViewDataSource

/// 禁用 section array
@property (nonatomic, strong) NSMutableArray <ZCPTableViewSectionDataModel *>*sectionDataModelArray NS_UNAVAILABLE;

/// section
@property (nonatomic, strong, readonly) ZCPTableViewSectionDataModel *singleSectionDataModel;
/// cell 视图模型数组
@property (nonatomic, strong) NSMutableArray *cellViewModelArray;

@end

NS_ASSUME_NONNULL_END
