//
//  ZCPTableViewSingleSectionDataSource.m
//  ZCPUIKit
//
//  Created by zhuchaopeng06607 on 2019/11/28.
//

#import "ZCPTableViewSingleSectionDataSource.h"

@implementation ZCPTableViewSingleSectionDataSource

@synthesize singleSectionDataModel = _singleSectionDataModel;

#pragma mark - override

- (ZCPTableViewSectionDataModel *)dataModelForSectionInSection:(NSInteger)section {
    if (section > 0) {
        return nil;
    }
    return self.singleSectionDataModel;
}

- (NSInteger)numberOfSections {
    return 1;
}

- (NSInteger)numberOfRowsInSection:(NSInteger)section {
    if (section > 0) {
        return nil;
    }
    NSInteger numberOfRows = self.singleSectionDataModel.cellViewModelArray.count;
    return numberOfRows;
}

#pragma mark - getters and setters

- (ZCPTableViewSectionDataModel *)singleSectionDataModel {
    if (!_singleSectionDataModel) {
        _singleSectionDataModel = [[ZCPTableViewSectionDataModel alloc] init];
    }
    return _singleSectionDataModel;
}

- (NSMutableArray *)cellViewModelArray {
    return self.singleSectionDataModel.cellViewModelArray;
}

@end
