//
//  ZCPTableViewCellViewModelProtocol.h
//  ZCPUIKit
//
//  Created by zhuchaopeng06607 on 2019/11/21.
//

#import <Foundation/Foundation.h>
@protocol ZCPTableViewCellProtocol;

NS_ASSUME_NONNULL_BEGIN

@protocol ZCPTableViewCellViewModelProtocol <NSObject>

/// cell 的类，用于实例化 cell
@property (nonatomic, strong) Class<ZCPTableViewCellProtocol> cellClass;
/// cell 重用标识，用于 cell 重用
@property (nonatomic, copy) NSString *cellReuseIdentifier;
/// cell 类型，用于区分 cell 的不同
@property (nonatomic, copy) NSString *cellType;
/// cell 标识，用于对 cell 进行标识
@property (nonatomic, assign) NSInteger cellTag;
/// cell高度，用于指定 cell 的高度
@property (nonatomic, strong) NSNumber *cellHeight;
/// cell 响应对象，用于响应 cell 的事件
@property (nonatomic, weak) id cellSelResponse;
/// 是否使用nib，默认为NO
@property (nonatomic, assign) BOOL useNib;
/// cell背景颜色
@property (nonatomic, strong) UIColor *cellBgColor;

/// 上边线是否隐藏，默认为YES
@property (nonatomic, assign) BOOL topSeparatorHidden;
/// 上边线颜色，如果传nil，则颜色默认为f1f1f1
@property (nonatomic, strong) UIColor *topSeparatorColor;
/// 上边线边距，仅left、right有效，默认为UIEdgeInsetsZero
@property (nonatomic, assign) UIEdgeInsets topSeparatorInset;
/// 下边线是否隐藏，默认为YES
@property (nonatomic, assign) BOOL bottomSeparatorHidden;
/// 下边线颜色，如果传nil，则颜色默认为f1f1f1
@property (nonatomic, strong) UIColor *bottomSeparatorColor;
/// 下边线边距，仅left、right有效，默认为UIEdgeInsetsZero
@property (nonatomic, assign) UIEdgeInsets bottomSeparatorInset;

@end

NS_ASSUME_NONNULL_END
