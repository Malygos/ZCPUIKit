//
//  ZCPTableViewCellViewModel.m
//  ZCPUIKit
//
//  Created by zhuchaopeng06607 on 2019/11/20.
//

#import "ZCPTableViewCellViewModel.h"

@implementation ZCPTableViewCellViewModel

#pragma mark - synthesize

@synthesize cellClass               = _cellClass;
@synthesize cellReuseIdentifier     = _cellReuseIdentifier;
@synthesize cellType                = _cellType;
@synthesize cellTag                 = _cellTag;
@synthesize cellHeight              = _cellHeight;
@synthesize cellSelResponse         = _cellSelResponse;
@synthesize useNib                  = _useNib;
@synthesize cellBgColor             = _cellBgColor;
@synthesize topSeparatorHidden      = _topSeparatorHidden;
@synthesize topSeparatorColor       = _topSeparatorColor;
@synthesize topSeparatorInset       = _topSeparatorInset;
@synthesize bottomSeparatorHidden   = _bottomSeparatorHidden;
@synthesize bottomSeparatorColor    = _bottomSeparatorColor;
@synthesize bottomSeparatorInset    = _bottomSeparatorInset;

#pragma mark - init

- (instancetype)init {
    if (self = [super init]) {
        self.cellClass              = NSClassFromString(@"ZCPTableViewCell");
        self.cellReuseIdentifier    = @"ZCPTableViewCell";
        self.topSeparatorHidden     = YES;
        self.bottomSeparatorHidden  = YES;
    }
    return self;
}

#pragma mark - NSCopying

- (instancetype)copyWithZone:(NSZone *)zone {
    ZCPTableViewCellViewModel *newViewModel = [[ZCPTableViewCellViewModel alloc] init];
    newViewModel.cellClass                  = self.cellClass;
    newViewModel.cellReuseIdentifier        = self.cellReuseIdentifier;
    newViewModel.cellType                   = self.cellType;
    newViewModel.cellTag                    = self.cellTag;
    newViewModel.cellHeight                 = self.cellHeight.copy;
    newViewModel.cellSelResponse            = self.cellSelResponse;
    newViewModel.useNib                     = self.useNib;
    newViewModel.cellBgColor                = self.cellBgColor.copy;
    newViewModel.topSeparatorHidden         = self.topSeparatorHidden;
    newViewModel.topSeparatorColor          = self.topSeparatorColor.copy;
    newViewModel.topSeparatorInset          = self.topSeparatorInset;
    newViewModel.bottomSeparatorHidden      = self.bottomSeparatorHidden;
    newViewModel.bottomSeparatorColor       = self.bottomSeparatorColor.copy;
    newViewModel.bottomSeparatorInset       = self.bottomSeparatorInset;
    return newViewModel;
}

@end
