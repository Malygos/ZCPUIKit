//
//  ZCPTableViewSectionViewModel.m
//  ZCPUIKit
//
//  Created by zhuchaopeng06607 on 2019/11/20.
//

#import "ZCPTableViewSectionViewModel.h"

@implementation ZCPTableViewSectionViewModel

#pragma mark - NSCopying

- (id)copyWithZone:(NSZone *)zone {
    ZCPTableViewSectionViewModel *newViewModel = [[ZCPTableViewSectionViewModel alloc] init];
    newViewModel.sectionViewClass = self.sectionViewClass;
    newViewModel.sectionViewHeight = self.sectionViewHeight.copy;
    newViewModel.sectionViewBgColor = self.sectionViewBgColor.copy;
    return newViewModel;
}

@end
