//
//  ZCPTableViewSectionDataModel.h
//  ZCPUIKit
//
//  Created by zhuchaopeng06607 on 2019/11/21.
//

#import <Foundation/Foundation.h>
#import "ZCPTableViewCellViewModel.h"
#import "ZCPTableViewSectionViewModel.h"

NS_ASSUME_NONNULL_BEGIN

/// section 数据模型
@interface ZCPTableViewSectionDataModel : NSObject <NSCopying>

/// header viewmodel
@property (nonatomic, strong) ZCPTableViewSectionViewModel *headerViewModel;
/// footer viewmodel
@property (nonatomic, strong) ZCPTableViewSectionViewModel *footerViewModel;
/// section下的cell
@property (nonatomic, strong) NSMutableArray <ZCPTableViewCellViewModel *> *cellViewModelArray;

@end

NS_ASSUME_NONNULL_END
