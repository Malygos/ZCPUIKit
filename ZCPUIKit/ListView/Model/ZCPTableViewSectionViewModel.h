//
//  ZCPTableViewSectionViewModel.h
//  ZCPUIKit
//
//  Created by zhuchaopeng06607 on 2019/11/20.
//

#import <Foundation/Foundation.h>
#import "ZCPTableViewSectionViewProtocol.h"

NS_ASSUME_NONNULL_BEGIN

/// section 视图模型
@interface ZCPTableViewSectionViewModel : NSObject <NSCopying>

/// section view 的类
@property (nonatomic, strong) Class <ZCPTableViewSectionViewProtocol> sectionViewClass;
/// section view 的高度
@property (nonatomic, strong) NSNumber *sectionViewHeight;
/// section view 背景颜色
@property (nonatomic, strong) UIColor *sectionViewBgColor;

@end

NS_ASSUME_NONNULL_END
