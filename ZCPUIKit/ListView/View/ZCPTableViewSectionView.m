//
//  ZCPTableViewSectionView.m
//  ZCPUIKit
//
//  Created by zhuchaopeng06607 on 2019/11/21.
//

#import "ZCPTableViewSectionView.h"

@implementation ZCPTableViewSectionView

@synthesize viewModel = _viewModel;

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor whiteColor];
    }
    return self;
}

#pragma mark - ZCPTableViewSectionViewProtocol

- (void)updateWithViewModel:(ZCPTableViewSectionViewModel *)viewModel {
    _viewModel = viewModel;
    
    if (viewModel.sectionViewBgColor) {
        self.backgroundColor = viewModel.sectionViewBgColor;
    }
}

@end
