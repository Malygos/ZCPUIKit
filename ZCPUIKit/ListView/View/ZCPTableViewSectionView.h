//
//  ZCPTableViewSectionView.h
//  ZCPUIKit
//
//  Created by zhuchaopeng06607 on 2019/11/21.
//

#import <UIKit/UIKit.h>
#import "ZCPTableViewSectionViewModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZCPTableViewSectionView : UIView <ZCPTableViewSectionViewProtocol>

@end

NS_ASSUME_NONNULL_END
