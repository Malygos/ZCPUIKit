//
//  ZCPTableViewSectionViewProtocol.h
//  ZCPUIKit
//
//  Created by zhuchaopeng06607 on 2019/11/21.
//

#import <Foundation/Foundation.h>
@class ZCPTableViewSectionViewModel;

NS_ASSUME_NONNULL_BEGIN

/// section 视图协议
@protocol ZCPTableViewSectionViewProtocol <NSObject>

/// 视图模型
@property (nonatomic, strong) ZCPTableViewSectionViewModel *viewModel;

/// 根据 viewmodel 更新 sectionView
/// @param viewModel 视图模型
- (void)updateWithViewModel:(ZCPTableViewSectionViewModel *)viewModel;

@end

NS_ASSUME_NONNULL_END
