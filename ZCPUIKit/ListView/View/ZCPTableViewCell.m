//
//  ZCPTableViewCell.m
//  ZCPUIKit
//
//  Created by zcp on 16/1/14.
//  Copyright © 2016年 zcp. All rights reserved.
//

#import "ZCPTableViewCell.h"

@implementation ZCPTableViewCell

#pragma mark - synthesize

@synthesize topSeparatorView    = _topSeparatorView;
@synthesize bottomSeparatorView = _bottomSeparatorView;
@synthesize viewModel           = _viewModel;

#pragma mark - init

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setupCell];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setupCell];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    CGSize cellSize = self.frame.size;
    CGFloat onePixel = 1.0/[UIScreen mainScreen].scale;
    
    if (!self.viewModel.topSeparatorHidden) {
        CGFloat left = self.viewModel.bottomSeparatorInset.left;
        CGFloat width = cellSize.width - left - self.viewModel.topSeparatorInset.right;
        self.topSeparatorView.frame = CGRectMake(left, 0, width, onePixel);
    }
    if (!self.viewModel.bottomSeparatorHidden) {
        CGFloat left = self.viewModel.bottomSeparatorInset.left;
        CGFloat width = self.frame.size.width - left - self.viewModel.bottomSeparatorInset.right;
        self.bottomSeparatorView.frame = CGRectMake(left, cellSize.height - onePixel, width, onePixel);
    }
}

#pragma mark - setup

- (void)setupCell {
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.accessoryType = UITableViewCellAccessoryNone;
    self.clipsToBounds = NO;
    self.contentView.clipsToBounds = NO;
    [self addSubview:self.topSeparatorView];
    [self addSubview:self.bottomSeparatorView];
    
    [self setupContentView];
    
    [self bringSubviewToFront:self.topSeparatorView];
    [self bringSubviewToFront:self.bottomSeparatorView];
}

#pragma mark - ZCPTableViewCellProtocol

+ (CGFloat)heightForViewModel:(ZCPTableViewCellViewModel *)viewModel {
    if (viewModel.cellHeight) {
        CGFloat height = viewModel.cellHeight.floatValue;
        return height;
    }
    return 0;
}

+ (instancetype)cellForViewModel:(ZCPTableViewCellViewModel *)viewModel {
    ZCPTableViewCell *cell  = nil;
    Class cellClass = viewModel.cellClass;
    NSString *reuseIdentifier = viewModel.cellReuseIdentifier;
    
    if (viewModel.useNib) {
        UINib *nib = [UINib nibWithNibName:NSStringFromClass(cellClass) bundle:[NSBundle bundleForClass:self]];
        cell = [[nib instantiateWithOwner:nil options:nil] objectAtIndex:0];
    } else {
        cell = [[cellClass alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    }
    return cell;
}

+ (NSString *)cellReuseIdentifier {
    return NSStringFromClass(self);
}

- (void)setupContentView {
}

- (void)updateWithViewModel:(ZCPTableViewCellViewModel *)viewModel {
    self.viewModel = viewModel;
    if (viewModel.cellBgColor && [viewModel.cellBgColor isKindOfClass:[UIColor class]]) {
        self.backgroundColor = viewModel.cellBgColor;
    } else {
        self.backgroundColor = [UIColor whiteColor];
    }
    
    if (!viewModel.topSeparatorHidden) {
        if (viewModel.topSeparatorColor && [viewModel.topSeparatorColor isKindOfClass:[UIColor class]]) {
            self.topSeparatorView.backgroundColor = viewModel.topSeparatorColor;
        } else {
            // f1f1f1
            self.topSeparatorView.backgroundColor = [UIColor colorWithRed:241.0/255 green:241.0/255 blue:241.0/255 alpha:1.0];
        }
    }
    if (!viewModel.bottomSeparatorHidden) {
        if (viewModel.bottomSeparatorColor && [viewModel.bottomSeparatorColor isKindOfClass:[UIColor class]]) {
            self.bottomSeparatorView.backgroundColor = viewModel.bottomSeparatorColor;
        } else {
            // f1f1f1
            self.bottomSeparatorView.backgroundColor = [UIColor colorWithRed:241.0/255 green:241.0/255 blue:241.0/255 alpha:1.0];
        }
    }
    self.topSeparatorView.hidden = viewModel.topSeparatorHidden;
    self.bottomSeparatorView.hidden = viewModel.bottomSeparatorHidden;
}

#pragma mark - getters and setters

- (UIView *)topSeparatorView {
    if (!_topSeparatorView) {
        _topSeparatorView = [[UIView alloc] init];
    }
    return _topSeparatorView;
}

- (UIView *)bottomSeparatorView {
    if (!_bottomSeparatorView) {
        _bottomSeparatorView = [[UIView alloc] init];
    }
    return _bottomSeparatorView;
}

@end
