//
//  ZCPTableViewCell.h
//  ZCPUIKit
//
//  Created by zcp on 16/1/14.
//  Copyright © 2016年 zcp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZCPTableViewCellProtocol.h"
#import "ZCPTableViewCellViewModel.h"

/// cell 基类
@interface ZCPTableViewCell : UITableViewCell <ZCPTableViewCellProtocol>

@end
