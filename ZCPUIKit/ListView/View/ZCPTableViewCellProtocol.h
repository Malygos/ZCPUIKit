//
//  ZCPTableViewCellProtocol.h
//  ZCPUIKit
//
//  Created by zhuchaopeng06607 on 2019/11/20.
//

#import <Foundation/Foundation.h>
#import "ZCPTableViewCellViewModelProtocol.h"

NS_ASSUME_NONNULL_BEGIN

/// cell 协议
@protocol ZCPTableViewCellProtocol <NSObject>

/// 顶部分割线
@property (nonatomic, strong) UIView *topSeparatorView;
/// 底部分割线
@property (nonatomic, strong) UIView *bottomSeparatorView;
/// 视图模型
@property (nonatomic, strong) id<ZCPTableViewCellViewModelProtocol> viewModel;

/// 根据 viewmodel 获得cell高度
/// @param viewModel 视图模型
+ (CGFloat)heightForViewModel:(id<ZCPTableViewCellViewModelProtocol>)viewModel;

/// 根据 viewmodel 实例化
/// @param viewModel 视图模型
+ (instancetype)cellForViewModel:(id<ZCPTableViewCellViewModelProtocol>)viewModel;

/// cell 重用标记
+ (NSString *)cellReuseIdentifier;

/// 初始化 contentview 的内容
- (void)setupContentView;

/// 根据 viewmodel 更新 cell
/// @param viewModel 视图模型
- (void)updateWithViewModel:(id<ZCPTableViewCellViewModelProtocol>)viewModel;

@end

NS_ASSUME_NONNULL_END
