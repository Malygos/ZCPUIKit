//
//  ZCPTableViewSingleTitleSectionView.h
//  ZCPUIKit
//
//  Created by zhuchaopeng06607 on 2019/11/21.
//

#import <UIKit/UIKit.h>
#import "ZCPTableViewSectionView.h"

NS_ASSUME_NONNULL_BEGIN

/// 单标题 section view
@interface ZCPTableViewSingleTitleSectionView : ZCPTableViewSectionView

@property (nonatomic, strong) UILabel *titleLabel;

@end

@interface ZCPTableViewSingleTitleSectionViewModel : ZCPTableViewSectionViewModel

/// 标题
@property (nonatomic, copy) NSString *titleString;
/// 标题字体
@property (nonatomic, strong) UIFont *titleFont;
/// 标题颜色
@property (nonatomic, strong) UIColor *titleColor;
/// 富文本标题
@property (nonatomic, strong) NSAttributedString *attributedTitleString;
/// 标题对齐方式
@property (nonatomic, assign) NSTextAlignment titleAlignment;
/// 标题外边距，默认为UIEdgeInsetsZero
@property (nonatomic, assign) UIEdgeInsets titleEdgeInsets;

@end

NS_ASSUME_NONNULL_END
