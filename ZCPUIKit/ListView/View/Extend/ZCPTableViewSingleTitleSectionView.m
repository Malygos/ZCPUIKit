//
//  ZCPTableViewSingleTitleSectionView.m
//  ZCPUIKit
//
//  Created by zhuchaopeng06607 on 2019/11/21.
//

#import "ZCPTableViewSingleTitleSectionView.h"
#import <Masonry.h>

@implementation ZCPTableViewSingleTitleSectionView

#pragma mark - init

- (instancetype)initWithFrame:(CGRect)frame {
    if ([super initWithFrame:frame]) {
        [self addSubview:self.titleLabel];
    }
    return self;
}

#pragma mark - override

- (void)updateWithViewModel:(ZCPTableViewSingleTitleSectionViewModel *)viewModel {
    [super updateWithViewModel:viewModel];
    
    // 这句代码会清掉text的相关信息，text、font、color等
    self.titleLabel.attributedText = nil;
    
    // 设置基本的text样式
    self.titleLabel.text = (viewModel.titleString.length > 0) ? viewModel.titleString : @"";
    self.titleLabel.font = viewModel.titleFont;
    self.titleLabel.textColor = viewModel.titleColor;
    self.titleLabel.textAlignment = viewModel.titleAlignment;
    
    // 当有富文本时会根据富文本中的内容覆盖对应的信息，text、font、color等。如富文本中未设置某个样式时，则会使用基本样式
    if (viewModel.attributedTitleString) {
        self.titleLabel.attributedText = viewModel.attributedTitleString;
    }
    
    [self.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(viewModel.titleEdgeInsets);
    }];
}

#pragma mark - getters and setters

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
    }
    return _titleLabel;
}

@end

@implementation ZCPTableViewSingleTitleSectionViewModel

- (instancetype)init {
    if (self = [super init]) {
        self.sectionViewClass = [ZCPTableViewSingleTitleSectionView class];
        self.sectionViewHeight = @20;
    }
    return self;
}

@end
