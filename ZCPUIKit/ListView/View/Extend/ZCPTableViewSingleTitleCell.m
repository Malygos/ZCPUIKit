//
//  ZCPTableViewSingleTitleCell.m
//  ZCPUIKit
//
//  Created by zhuchaopeng06607 on 2019/11/21.
//

#import "ZCPTableViewSingleTitleCell.h"
#import <Masonry.h>

@implementation ZCPTableViewSingleTitleCell

#pragma mark - override

- (void)setupContentView {
    [self.contentView addSubview:self.titleLabel];
}

- (void)updateWithViewModel:(ZCPTableViewSingleTitleCellViewModel *)viewModel {
    [super updateWithViewModel:viewModel];
    
    // 这句代码会清掉text的相关信息，text、font、color等
    self.titleLabel.attributedText = nil;
    
    // 设置基本的text样式
    self.titleLabel.text = (viewModel.titleString.length > 0) ? viewModel.titleString : @"";
    self.titleLabel.font = viewModel.titleFont;
    self.titleLabel.textColor = viewModel.titleColor;
    self.titleLabel.textAlignment = viewModel.titleAlignment;
    self.titleLabel.numberOfLines = viewModel.titleNumberOfLines;
    
    // 当有富文本时会根据富文本中的内容覆盖对应的信息，text、font、color等。如富文本中未设置某个样式时，则会使用基本样式
    if (viewModel.attributedTitleString) {
        self.titleLabel.attributedText = viewModel.attributedTitleString;
    }
    
    [self.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(viewModel.titleEdgeInsets);
    }];
}

#pragma mark - getters and setters

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
    }
    return _titleLabel;
}

@end

@implementation ZCPTableViewSingleTitleCellViewModel

- (instancetype)init {
    if (self = [super init]) {
        self.cellClass = [ZCPTableViewSingleTitleCell class];
        self.cellReuseIdentifier = [ZCPTableViewSingleTitleCell cellReuseIdentifier];
        self.cellHeight = @50;
    }
    return self;
}

@end
