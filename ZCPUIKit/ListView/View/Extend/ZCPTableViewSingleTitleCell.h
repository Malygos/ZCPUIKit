//
//  ZCPTableViewSingleTitleCell.h
//  ZCPUIKit
//
//  Created by zhuchaopeng06607 on 2019/11/21.
//

#import "ZCPTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

/// 单标题 cell
@interface ZCPTableViewSingleTitleCell : ZCPTableViewCell

@property (nonatomic, strong) UILabel *titleLabel;

@end

/// 单标题 cell viewmodel
@interface ZCPTableViewSingleTitleCellViewModel : ZCPTableViewCellViewModel

/// 标题
@property (nonatomic, copy) NSString *titleString;
/// 标题字体
@property (nonatomic, strong) UIFont *titleFont;
/// 标题颜色
@property (nonatomic, strong) UIColor *titleColor;
/// 富文本标题
@property (nonatomic, strong) NSAttributedString *attributedTitleString;
/// 标题对齐方式
@property (nonatomic, assign) NSTextAlignment titleAlignment;
/// 标题最大行数，默认为0
@property (nonatomic, assign) NSInteger titleNumberOfLines;
/// 标题外边距，默认为UIEdgeInsetsZero
@property (nonatomic, assign) UIEdgeInsets titleEdgeInsets;

@end

NS_ASSUME_NONNULL_END
