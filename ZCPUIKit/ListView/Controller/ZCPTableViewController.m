//
//  ZCPTableViewController.m
//  ZCPUIKit
//
//  Created by zhuchaopeng on 16/9/21.
//  Copyright © 2016年 zcp. All rights reserved.
//

#import "ZCPTableViewController.h"

@implementation ZCPTableViewController

#pragma mark - life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view addSubview:self.tableView];
    [self constructData];
    [self.tableView reloadData];
}

#pragma mark - public

- (void)constructData {
}

#pragma mark - UITableViewDelegate

UITableViewDelegate_ZCP_BASE_IMP(self.tableViewDataSource)

#pragma mark - getters and setters

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.backgroundView = nil;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.estimatedRowHeight = 0;
        _tableView.estimatedSectionHeaderHeight = 0;
        _tableView.estimatedSectionFooterHeight = 0;
        _tableView.dataSource = self.tableViewDataSource;
        _tableView.delegate = self;
    }
    return _tableView;
}

- (ZCPTableViewDataSource *)tableViewDataSource {
    if (!_tableViewDataSource) {
        _tableViewDataSource = [[ZCPTableViewDataSource alloc] init];
    }
    return _tableViewDataSource;
}

@end
