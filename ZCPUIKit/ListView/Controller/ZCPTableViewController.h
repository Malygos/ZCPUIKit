//
//  ZCPTableViewController.h
//  ZCPUIKit
//
//  Created by zhuchaopeng on 16/9/21.
//  Copyright © 2016年 zcp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZCPTableViewDataSource.h"

/// tableView 视图控制器
@interface ZCPTableViewController : UIViewController <UITableViewDelegate>

/// tableView
@property (nonatomic, strong) UITableView *tableView;
/// tableView 数据源
@property (nonatomic, strong) ZCPTableViewDataSource *tableViewDataSource;

/// 构造数据
- (void)constructData;

@end
