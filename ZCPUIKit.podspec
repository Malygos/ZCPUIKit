Pod::Spec.new do |s|

  s.name         = "ZCPUIKit"
  s.version      = "0.0.1"
  s.summary      = "It`s my ui kit."
  s.description  = <<-DESC
                      It`s my ui kit. It has some ui util.
                   DESC

  s.homepage     = "https://gitlab.com/Malygos/ZCPUIKit"
  s.license      = { :type => 'MIT', :file => 'LICENSE' }
  s.author       = { "朱超鹏" => "z164757979@163.com" }
  s.source       = { :git => "https://gitlab.com/Malygos/ZCPUIKit.git", :tag => "#{s.version}" }
  s.platform     = :ios, '8.0'

  s.source_files = "ZCPUIKit/ZCPUIKit.h"
  s.public_header_files = "ZCPUIKit/ZCPUIKit.h"
  s.framework    = 'Foundation', 'UIKit'

# ――― Subspec ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

  s.subspec 'ListView' do |ss|
    ss.source_files = 'ZCPUIKit/ListView/**/*.{h,m}'
    ss.dependency 'Masonry'
  end

  s.subspec 'Extend' do |ss|
    ss.dependency 'ZCPUIKit/ListView'
    ss.source_files = 'ZCPUIKit/Extend/**/*.{h,m}'
  end
  
end
